const express = require('express')
const app = express()
const MongoClient = require('mongodb').MongoClient
const mongourl = "mongodb+srv://cca-jiangh8:jhw888@cca-jiangh8.avq5f.mongodb.net/cca-labs?retryWrites=true&w=majority"
const dbClient = new MongoClient(mongourl,{useNewUrlParser:true,useUnifiedTopology:true})
var port = process.env.PORT || 8080
app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
app.listen(port)
console.log("US City Search Microservice is running on port "+port)
dbClient.connect(err=>{
    if(err) throw err
    console.log("Connected to the MongoDB cluster")
})
app.get("/",(req,res)=>{
    res.send("US City Search Microservice by Hanwen Jiang")
})
let fields = {_id: false,
                    zips:true,
                    city:true,
                    state_id: true,
                    state_name:true,
                    county_name:true,
                    timezone:true}
app.get('/uscities-search/:zips(\\d{1,5})', function(req,res){
    const db = dbClient.db();
    let zipRegEx = new RegExp(req.params.zips)
    const cursor = db.collection("uscities").find({zips:zipRegEx}).project(fields)
    cursor.toArray(function(err, results){
        console.log(results)
        res.send(results)
    })
})
app.get('/uscities-search/:city',function(req,res){
    const db = dbClient.db()
    let cityRegEx = new RegExp(req.params.city,'i')
    const cursor = db.collection('uscities').find({city:cityRegEx}).project(fields)
    cursor.toArray(function(err,results){
        console.log(results)
        res.send(results)
    })
})