/*const express = require('express')
const app = express()
var port = process.env.PORT || 8080
const server = require('http').createServer(app)
const io = require('socket.io')(server)
app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
server.listen(port, ()=>
    console.log(`WebChat Server is running on port:${port}`)
)
app.get('/',(req,res)=>{
    res.sendFile(__dirname+ '/static/chatclient.html')
})*/
const express= require('express');
const app=express();
var port = process.env.PORT || 8080; // important for deployment later
const server = require('http').createServer(app);
const io = require('socket.io')(server);
server.listen(port); //changed from app.listen(port)
app.use(express.static('static'));
app.use(express.urlencoded({extended: false}));
console.log("WebChat server is running on port " + port);
app.get("/", (req,res)=>{
    res.sendFile(__dirname + '/static/chatclient.html')
})
var client = []
io.on('connection',(socketclient)=>{
    //console.log('A new client is connected!')
    var welcomemessage = `${socketclient.id} is connected! Number of connected clients: ${client.push(socketclient.id)}`
    console.log(welcomemessage)
    io.emit("online", welcomemessage)
    socketclient.on("disconnect", ()=>{
        client.pop(socketclient.id)
        var byemessage = `${socketclient.id} is disconnected! Number of connected clients: ${client.length}`
        console.log(byemessage)
        io.emit("online", byemessage)

    })
    socketclient.on("message", (data) => {
        console.log('Data from a client: ' + data)
        io.emit("message", `${socketclient.id} says: ${data}`)
    })
    socketclient.on("typing", () => {
        console.log('Someone is typing...')
        io.emit("typing", `${socketclient.id} is typing ...`)
    })
   
})
