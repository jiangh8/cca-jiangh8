const express= require('express');
const app=express();
var port = process.env.PORT || 8080; // important for deployment later
const server = require('http').createServer(app);
const https= require('https')
const querystring = require('querystring')
const io = require('socket.io')(server);
server.listen(port); //changed from app.listen(port)
app.use(express.static('static'));
app.use(express.urlencoded({extended: false}));
console.log("WebChat server is running on port " + port);
app.get("/", (req,res)=>{
    res.sendFile(__dirname + '/static/chatclient.html')
})
var client = []
io.on('connection',(socketclient)=>{
    //console.log('A new client is connected!')
    //io.emit("online", welcomemessage)
    socketclient.on("login",(username,password)=>{
        console.log(`Debug> get user data: ${username} and ${password}`)
        login(username,password,(authenticated)=>{
            if(authenticated)
            {
                var onlineclients = Object.keys(io.sockets.sockets).length
                socketclient.authenticated = true
                socketclient.username = username
                var welcomemessage = `${username} is connected! Number of connected clients: ${onlineclients}`
                socketclient.emit("authenticated")
                console.log(`Debug> ${username} is authenticated`)
                BroadcastAuthenticatedClients("welcome",welcomemessage)
            }
            else
            {
                socketclient.authenticated = false
                socketclient.emit("login-failed")
                console.log(`Debug> Login failed: ${username}/${password}`)
            }
        })
        
    })
    socketclient.on("register",(username,password,phoneNumber)=>{
        console.log(`Debug> get user data: ${username} , ${password} and ${phoneNumber}`)
        var postdata = querystring.stringify({'username':username, 'password':password,'phoneNumber':phoneNumber})
        var options = {
            hostname: 'cca-jiangh8-microservice.herokuapp.com',
            port: 443,
            path: '/signup',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': postdata.length
            }
        }
        var req = https.request(options, (res)=>{
            let data = ''
            res.on('data', (chunk)=>{
                data += chunk
            })
            res.on('end',()=>{
                const result = JSON.parse(data);
                if(!result || result.status==="duplicate username")
                {
                    console.log(`${username} has already exist`)
                    socketclient.emit("username exist")
                }
                else
                {
                    console.log(`${username} has signed up successfully!`)
                    socketclient.emit("signup success")
                }
            })         
        })
            req.write(postdata)
            req.end()
    })
    socketclient.on("message", (data) => {
        if(!socketclient.authenticated)
        {
            console.log("unknown user send a message..")
            return
        }
        console.log('Data from a client: ' + data)
        BroadcastAuthenticatedClients("message", `${socketclient.username} says: ${data}`)
    })
    socketclient.on("typing", () => {
        if(!socketclient.authenticated)
        {
            return
        }
        console.log('Someone is typing...')
        BroadcastAuthenticatedClients("typing", `${socketclient.username} is typing ...`)
    })
   
})

function login(username,password,callback){
    https.get('https://cca-jiangh8-microservice.herokuapp.com/login/'+username+'/'+password,(response)=>{
        let data = ''
        response.on('data',(chunk)=>{
            data += chunk
        })
        response.on('end',()=>{
            const result = JSON.parse(data);
            if(!result || result.status==="failed")
            {
                callback(false)
            }
            else
            {
                callback(true)
            }
        })
    })


}
function BroadcastAuthenticatedClients(event,message){
    var sockets = io.sockets.sockets
    for(var id in sockets)
    {
        const socketclient = sockets[id]
        if(socketclient&&socketclient.authenticated)
        {
            socketclient.emit(event,message)
        }
    }
}