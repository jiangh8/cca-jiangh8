const express = require('express')
const app = express()
const MongoClient = require('mongodb').MongoClient
const mongourl = "mongodb+srv://cca-jiangh8:jhw888@cca-jiangh8.avq5f.mongodb.net/cca-labs?retryWrites=true&w=majority"
const dbClient = new MongoClient(mongourl,{useNewUrlParser:true,useUnifiedTopology:true})
var port = process.env.PORT || 8080
app.use(express.static('static'))
app.use(express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
app.listen(port)
console.log("US City Search Microservice is running on port "+port)
dbClient.connect(err=>{
    if(err) throw err
    console.log("Connected to the MongoDB cluster")
})
app.get("/",(req,res)=>{
    res.send("Registration and Login Microservice by Hanwen Jiang")
})

app.get('/login/:username/:password', function(req,res){
    var username = req.params.username
    var password = req.params.password
    console.log('get username: '+username)
    console.log('get password: '+password)
    const db = dbClient.db()
    db.collection("users").findOne({username:username,password:password}, (err, user)=>{
        if(err|| !user)
        {
            console.log(`${username}/${password} not found!`)
            res.send(JSON.stringify({status:"failed"}))
        }
        if(user&&user.username===username)
        {
            console.log(`${username}/${password} found!`)
            res.send(JSON.stringify({status:"found"}))
        }
    })
})
app.post('/signup',(req,res) =>{
    const {username,password,phoneNumber} = req.body
    console.log('/signup->req.body.username: ' + username)
    console.log('/signup->req.body.password: ' + password)
    const db = dbClient.db()
    db.collection("users").findOne({username: username}, (err,user)=>{
        if(user)
        {
            res.send(JSON.stringify({status:"duplicate username"}))
        }
        else
        {
            let newUser = {username:username, password:password,phoneNumber:phoneNumber}
            db.collection("users").insertOne(newUser, (err, result)=>{
                if(err)
                {
                    res.send(JSON.stringify({status:"failed"}))
                }
                res.send(JSON.stringify({status:"success"}))
            })
        }
    })
})